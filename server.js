/* IoTool chat authorization server */
/* Node modules */
var express = require('express');
var json = require('express-json');
var bb = require('express-busboy');
var http = require('http');
var fs = require('fs');
var config = require('config');
var cors = require('cors');

/* Own modules */
var userAuth = require('./userAuth.js');
var xmppUtils = require('./xmppUtils.js');
var tokenAuth = require('./tokenAuth.js');
var utils = require('./utils.js');

var hostname = config.get('Server.hostname');
var port = config.get('Server.port');
var app = express();
app.use(cors());
bb.extend(app);

app.post('/get_token', function(req, res, next) {
    if(utils.requestMalformed(req)) {
	return next(new utils.RequestError());
    }

    var data = req.body;
    var username = data.u.toLowerCase();
    var password = data.p;
    var contacts = data.c;

    function errorHandler(error) {
	switch (error.name) {
	case 'UserNullError':
	    createUserAndSendToken();
	    break;
	case 'PasswordError':
	    res.sendStatus(401);
	    break;
	default:
	    console.log(error);
	    res.sendStatus(500);
	    break;
	}
    }

    function createAndSendToken() {
	tokenAuth.createToken(errorHandler, function(data) {
	    res.send(data);
	}, username, password, contacts);
    }

    function createUserAndSendToken() {
	var params = [errorHandler, createAndSendToken, username, password];
	xmppUtils.createUser.apply(this, params);
    }

    var params = [errorHandler, createAndSendToken, username, password];
    userAuth.authorizeUser.apply(this, params);
}, function(error, req, res, next) {
    res.sendStatus(400);
});

app.post('/verify', function(req, res, next) {
    // Application expects JSON object with token stored in t
    var token = req.body.t;

    function errorHandler(error) {
	switch(error.name) {
	case 'TokenExpiredError':
	    res.sendStatus(401);
	    break;
	case 'TokenError':
	    res.sendStatus(401);
	    break;
	case 'JsonWebTokenError':
	    res.sendStatus(400);
	    break;
	default:
	    console.log(error);
	    res.sendStatus(500);
	    break;
	}
    }

    function success(credentials) {
	res.send(credentials);
    }

    tokenAuth.verifyToken(errorHandler, success, token);
});

app.post('/jwt', function(req, res, next) {
    if(utils.requestMalformed(req, next)) {
	return next(new utils.RequestError());
    }

    var username = req.body.username;
    var password = req.body.password;

    function errorHandler(error) {
	switch(error.name) {
	case 'PasswordError':
	    res.sendStatus(401);
	    break;
	case 'UserNullError':
	    res.sendStatus(401);
	    break;
	default:
	    console.log(error);
	    break;
	}
    }

    function success(token) {
	res.send(token);
    }

    tokenAuth.createTurnToken(errorHandler, success, username, password);
}, function(error, req, res, next) {
    res.sendStatus(400);
});

app.post('/turn_credentials', function(req, res, next) {
    if(utils.requestMalformed(req, next)) {
	return next(new utils.RequestError());
    }

    var token = req.headers.authorization;

    function errorHandler(error) {
	switch(error.name) {
	case 'JsonWebTokenError':
	    res.sendStatus(401);
	    break;
	case 'TokenExpiredError':
	    res.sendStatus(401);
	    break;
	default:
	    console.log(error);
	    res.sendStatus(500);
	    break;
	}
    }

    function success(config) {
	res.send(JSON.stringify(config));
    }

    tokenAuth.verifyTurnToken(errorHandler, success, token);
}, function(error, req, res, next) {
    res.sendStatus(400);
});

app.get('/', function(req, res) {
    res.sendStatus(200);
});

app.post('/create_user', function(req, res, next) {
    if(utils.requestMalformed(req, next)) {
	return next(new utils.RequestError());
    }

    var username = req.body.u;
    var password = req.body.p;

    function errorHandler(error) {
	if (error.message === 'Registration error') {
	    res.sendStatus(409);
	} else {
	    console.log(error);
	    res.sendStatus(500);
	}
    }

    function success() {
	res.sendStatus(200);
    }

    xmppUtils.createUser(errorHandler, success, username, password);
}, function(err, req, res, next) {
    res.sendStatus(400);
});

app.post('/delete_user', function(req, res, next) {
    if(utils.requestMalformed(req, next)) {
	return next(new utils.RequestError());
    }

    var username = req.body.u;
    var password = req.body.p;

    function errorHandler(error) {
	function onUserExists(exists) {
	    if (exists) {
		res.sendStatus(401);
	    } else {
		res.sendStatus(404);
	    }
	}

	if (error === 'XMPP authentication failure') {
	    userAuth.checkIfUserExists(errorHandler, onUserExists, username);
	} else if (error.message !== 'remote-stream-error') {
	    console.log(error);
	    res.sendStatus(500);
	}
    }

    function onUserDeleted() {
	res.sendStatus(200);
    }

    xmppUtils.deleteUser(errorHandler, onUserDeleted, username, password);
}, function(err, req, res, next) {
    res.sendStatus(400);
});

this.server = http.createServer(app);
this.server.listen(port, hostname);
console.log('Server listening on http://' + hostname + ':' + port);

exports.listen = function() {
    this.server.listen.apply(this.server, arguments);
};

exports.close = function(callback) {
    this.server.close(callback);
};

