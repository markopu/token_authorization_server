/* Utility functions and helpers */
var crypto = require('crypto');
var config = require('config');

function TokenError() {
    this.name = 'TokenError';
    this.message = 'token blacklisted';
}

function RequestError() {
    this.name = 'RequestError';
    this.message = 'malformed request';
}

function UserNullError() {
    this.name = 'UserNullError';
    this.message = 'User does not exist.';
}

function PasswordError() {
    this.name = 'PasswordError';
    this.message = 'Password mismatch';
}

function requestMalformed(req) {
    var isEmptyObject = Object.keys(req.body).length === 0 &&
	req.body.constructor === Object;
    return isEmptyObject ? true : false;
}

function generateWebRTCConf(username) {
    /* TODO make token expire at samte time as turn credentials */
    var webrtcConfig = config.get('webrtcConfig');
    var seconds = parseInt(Date.now() / 1000) + webrtcConfig.ttl;
    var temp_username = seconds.toString() + ':' + username;
    var hmac = crypto.createHmac('sha1', webrtcConfig.turnSecret);

    hmac.setEncoding('base64');
    hmac.write(temp_username);
    hmac.end();

    return {
	username: temp_username,
	password: hmac.read(),
	ttl: webrtcConfig.ttl,
	uris: webrtcConfig.turnServers,
	stun_uris: webrtcConfig.stunServers
    };
}

exports.TokenError = TokenError;
exports.RequestError = RequestError;
exports.UserNullError = UserNullError;
exports.PasswordError = PasswordError;
exports.requestMalformed = requestMalformed;
exports.generateWebRTCConf = generateWebRTCConf;
