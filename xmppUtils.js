var Client = require('node-xmpp-client');
var config = require('config');

var xmpp_conf = config.get('xmppConfig');

function createUser(error, callback, username, password) {
    var client = new Client({
	jid: username + '@' + xmpp_conf.domain_name,
	password: password,
	bosh: {
	    url: xmpp_conf.bosh_url,
	    wait: 60
	},
	register: true,
    });

    client.on('online', function() {
	callback();
	client.end();
    });

    client.on('error', function(err) {
	error(err);
	client.end();
    });
}

function deleteUser(error, callback, username, password) {
    var client = new Client({
	jid: username + '@' + xmpp_conf.domain_name,
	password: password,
	bosh: {
	    url: xmpp_conf.bosh_url,
	    wait: 60
	},
	register: false,
    });

    client.on('online', function() {
	var cancelRegistration = new Client.Element('iq', {
	    type: 'set'
	}).c('query', {
	    xmlns: 'jabber:iq:register'
	}).c('remove');

	client.send(cancelRegistration);
	callback();
	client.end();
    });

    client.on('error', function(err) {
	error(err);
	client.end();
    });
}

exports.createUser = createUser;
exports.deleteUser = deleteUser;
