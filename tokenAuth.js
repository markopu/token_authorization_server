/**
 * JWT based one time token authentication mechanism
 * using redis as temporary blacklist for used tokens 
*/

var jwt = require('jsonwebtoken');
var uuid = require('uuid');
var redis = require('redis');
var ms = require('ms');
var config = require('config');

var userAuth = require('./userAuth.js');
var utils = require('./utils.js');

var client = redis.createClient();

client.on('error', function(err) {
    console.log('redis error:', err);
});

var SECRET_KEY = config.get('AuthToken').secret;
var EXPIRES_IN = config.get('AuthToken').expiresIn;

function redisSetJsonExpire(error, callback, key, data, ex) {
    client.set(key, JSON.stringify(data), function(err, res) {
	if (err) return error(err);

	client.expire(key, ex, function(err, res) {
	    if (err) return error(err);
	    callback();
	});
    });
}

/**
 * Create JWT token, payload consists of UID that is used as 
 * key for lookups in the redis data store. Each key holds a 
 * JSON object containing username, password, and blacklisted 
 * property.
 */
function createToken(error, callback, username, password, contacts) {
    var tokenReady = false;
    var redisReady = false;
    var tokenData;
    
    var id = uuid.v1();
    var credentials = {
	u: username,
	p: password,
	c: contacts,
	b: 0
    };

    function ready() {
	if (tokenReady && redisReady) {
	    callback(tokenData);
	}
    }
    
    var token = jwt.sign({uuid: id}, SECRET_KEY, {
	expiresIn: EXPIRES_IN
    }, function(err, data) {
	if (err) return error(err);
	tokenReady = true;
	tokenData = data;
	ready();
    });

    redisSetJsonExpire(error, function() {
    	redisReady = true;
	ready();
    }, id, credentials, ms(EXPIRES_IN) / 1000);
}

function verifyToken(error, callback, token) {
    var id;
    var ttl;

    function gotJsonData(err, res) {
	if (err) return error(err);
	if (res === null) return error(new utils.TokenError());

	var data = JSON.parse(res);

	// Check for blacklisted flag
	if (data.b === 0) {

	    // Set blacklisted flag
	    data.b = 1;

	    // Update entry in redis
	    redisSetJsonExpire(error, function() {
		callback({u: data.u, p: data.p, c: data.c});
	    }, id, data, ttl);
	} else {
	    error(new utils.TokenError());
	}
    }

    function gotTtl(err, res) {
	if (err) return error(err);
	
	ttl = res;
	client.get(id, gotJsonData);
    }
    
    jwt.verify(token, SECRET_KEY, function(err, decoded) {
	if (err) return error(err);
	
	id = decoded.uuid;
	client.ttl(id, gotTtl);
    });
}

function createTurnToken(error, callback, username, password) {
    function onAuthorized() {
	var token = jwt.sign({name: username}, SECRET_KEY, {
	    expiresIn: config.get('webrtcConfig').ttl.toString() + 's'
	}, function(err, data) {
	    if (err) return error(err);
	    callback(data);
	});
    }
    
    userAuth.authorizeUser(error, onAuthorized, username, password);
}

function verifyTurnToken(error, callback, token) {
    token = token.substring(token.indexOf(' ') + 1);
    jwt.verify(token, SECRET_KEY, function(err, decoded) {
	if (err) return error(err);
	var username = decoded.name;
	callback(utils.generateWebRTCConf(username));
    });
}

exports.verifyToken = verifyToken;
exports.createToken = createToken;
exports.createTurnToken = createTurnToken;
exports.verifyTurnToken = verifyTurnToken;



























