/* jshint esversion: 6 */
var assert = require('assert');
var http = require('http');
var server = require('../server.js');

var test_token;
var invalid_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1dWlkIjoiYWI2MjFjODAtMzQ4NC0xMWU2LThjYmYtZGZmYzdiOTg3NWQ3IiwiaWF0IjoxNDY2MTY1NTA5LCJleHAiOjE0NjYxNjU1Njl9.sPRdbux0HebRo75_3TR9Z5sYK3TErr_bHE3ety6syYM';
var turn_token;

describe('server', () => {
    before(() => {
	server.listen(1337);
    });

    after(() => {
	server.close();
    });
});

describe('/', function() {
    it('should return 200', (done) => {
	http.get('http://localhost:1337', (res) => {
	    assert.equal(200, res.statusCode);
	    done();
	});
    });
});

function jsonPostRequest(data, auth, path, callback) {
    var resData = '';
    var req = http.request({
	port: 1337,
	method: 'POST',
	path: path,
	headers: {
	    'Content-type': 'application/json',
	    'Content-length': data.length,
	    'Authorization': auth
	}
    }, (res) => {
	res.setEncoding('utf8');
	res.on('data', (chunk) => {
	    resData += chunk;
	});
	res.on('end', () => {
	    callback(res, resData);
	});
    });
    
    req.write(data);
}

describe('/get_token', () => {
    it('should return token on existing user', (done) => {
	var requestData = {
	    u: 'test',
	    p: '12345678',
	    c: []
	};
	
	jsonPostRequest(JSON.stringify(requestData), '', '/get_token', (res, data) => {
	    assert.equal(res.statusCode, 200);
	    assert.ok(data);
	    test_token = data;
	    done();
	});
    });

    it('should return 401 on wrong password', (done) => {
	var requestData = {
	    u: 'test',
	    p: '123456789',
	    c: []
	};
	
	jsonPostRequest(JSON.stringify(requestData), '', '/get_token', (res, data) => {
	    assert.equal(res.statusCode, 401);
	    done();
	});
    });

    it('should return 400 on malformed request', (done) => {
	var requestData = {
	    u: 'test',
	    p: '123456789',
	    c: []
	};

	jsonPostRequest(JSON.stringify(requestData) + 'a', '', '/get_token', (res, data) => {
	    assert.equal(res.statusCode, 400);
	    done();
	});
    });


    /* TEST SPARINGLY TO PREVENT SERVER USENAME POLLUTION
    it('should return token on new user', function(done) {
	var requestData = {
	    u: parseInt(Math.random() * 100000 + 1).toString(),
	    p: 'random_password',
	    c: {}
	};

	jsonPostRequest(JSON.stringify(requestData), '/get_token', (res, data) => {
	    assert.equal(res.statusCode, 200);
	    assert.ok(data);
	    done();
	});
    });
    */
});

describe('/verify', () => {
    it('should return credentials on valid token', (done) => {
	var requestData = {
	    t: test_token
	};

	jsonPostRequest(JSON.stringify(requestData), '', '/verify', (res, data) => {
	    var credentials = JSON.parse(data);
	    assert.equal(res.statusCode, 200);
	    assert.equal(credentials.u, 'test');
	    assert.equal(credentials.p, '12345678');
	    assert.equal(credentials.c.length, 0);
	    done();
	});
    });

    it('should not allow token reuse', (done) => {
	var requestData = {
	    t: test_token
	};

	jsonPostRequest(JSON.stringify(requestData), '', '/verify', (res, data) => {
	    assert.equal(res.statusCode, 401);
	    done();
	});
    });
    
    it('should return 401 on invalid token', (done) => {
	var requestData = {
	    t: invalid_token
	};

	jsonPostRequest(JSON.stringify(requestData), '', '/verify', (res, data) => {
	    assert.equal(res.statusCode, 401);
	    done();
	});
    });
});


describe('/jwt', () => {
    it('should return token on correct credentials', (done) => {
	var requestData = {
	    username: 'test',
	    password: '12345678'
	};
	
	jsonPostRequest(JSON.stringify(requestData), '', '/jwt', (res, data) => {
	    assert.equal(res.statusCode, 200);
	    turn_token = data;
	    assert.ok(data);
	    done();
	});
    });

    it('should not return token on password mismatch', (done) => {
	var requestData = {
	    username: 'test',
	    password: '123456789'
	};
	
	jsonPostRequest(JSON.stringify(requestData), '', '/jwt', (res, data) => {
	    assert.equal(res.statusCode, 401);
	    done();
	});
    });

    it('should not return token on missing user', (done) => {
	var requestData = {
	    username: 'testmissinguser',
	    password: '123456789'
	};
	
	jsonPostRequest(JSON.stringify(requestData), '', '/jwt', (res, data) => {
	    assert.equal(res.statusCode, 401);
	    done();
	});
    });

    it('should return 400 on malformed request', (done) => {
	var requestData = {
	    username: 'test',
	    password: '12345678'
	};
	
	jsonPostRequest(JSON.stringify(requestData) + 'a', '', '/jwt', (res, data) => {
	    assert.equal(res.statusCode, 400);
	    done();
	});
    });
});

describe('/turn_credentials', () => {
    it('should return webrtc configuration on valid token', (done) => {
	var requestData = {
	    username: 'test'
	};
	var token = 'Bearer ' + turn_token;

	jsonPostRequest(JSON.stringify(requestData), token, '/turn_credentials', (res, data) => {
	    assert.equal(res.statusCode, 200);
	    assert.ok(data);
	    done();
	});
    });

    it('should not return webrtc configuration on invalid token', (done) => {
	var requestData = {
	    username: 'test'
	};
	var token = 'Bearer ' + invalid_token;

	jsonPostRequest(JSON.stringify(requestData), token, '/turn_credentials', (res, data) => {
	    assert.equal(res.statusCode, 401);
	    done();
	});
    });
});

describe('/create_user', (done) => {
    var requestData = {
	u: 'deletiontest',
	p: 'deletionpassword'
    };
    
    it('should return 200 on successful creation', (done) => {
	jsonPostRequest(JSON.stringify(requestData), '', '/create_user', (res, data) => {
	    assert.equal(res.statusCode, 200);
	    done();
	});
    });

    it('should return 409 if trying to create existing user', (done) => {
	jsonPostRequest(JSON.stringify(requestData), '', '/create_user', (res, data) => {
	    assert.equal(res.statusCode, 409);
	    done();
	});
    });

    it('should return 400 if request is malformed', (done) => {
	jsonPostRequest(JSON.stringify(requestData) + 'a', '', '/create_user', (res, data) => {
	    assert.equal(res.statusCode, 400);
	    done();
	});
    });
});

describe('/delete_user', (done) => {
    var requestData = {
	u: 'deletiontest',
	p: 'deletionpassword'
    };

    it('should return 401 on wrong user password', (done) => {
	var requestData = {
	    u: 'deletiontest',
	    p: 'wrongpassword'
	};
	
	jsonPostRequest(JSON.stringify(requestData), '', '/delete_user', (res, data) => {
	    assert.equal(res.statusCode, 401);
	    done();
	});
    });
    
    it('should return 200 on successful deletion', (done) => {
	jsonPostRequest(JSON.stringify(requestData), '', '/delete_user', (res, data) => {
	    assert.equal(res.statusCode, 200);
	    done();
	});
    });

    it('should return 404 when trying to delete non existing account', (done) => {
	var requestData = {
	    u: 'notdeletiontest',
	    p: 'wrongpassword'
	};
	
	jsonPostRequest(JSON.stringify(requestData), '', '/delete_user', (res, data) => {
	    assert.equal(res.statusCode, 404);
	    done();
	});
    });

    it('should return 400 if request is malformed', (done) => {
	jsonPostRequest(JSON.stringify(requestData) + 'a', '', '/delete_user', (res, data) => {
	    assert.equal(res.statusCode, 400);
	    done();
	});
    });
});
