/* Node modules */
var pg = require('pg');
var config = require('config');
var utils = require('./utils.js');

/* Configuration */
var db_config = config.get('xmppDbConfig');
var connection_string = 'postgres://' + db_config.user + ':' +
    db_config.password + '@' + db_config.host + ':' + db_config.port +
    '/' + db_config.database;

function getPassword(error, callback, username) {
    var sql = 'SELECT value FROM prosody WHERE "user"=\'' + username + '\' AND "key"=\'password\'';

    pg.connect(connection_string, function(err, client, done) {
	if (err) return error(err);
	
	client.query(sql, function(err, result) {
	    done();
	    if (err) return error(err);

	    if (typeof result.rows[0] === 'undefined') {
		error(new utils.UserNullError());
	    } else {
		callback(result.rows[0].value);
	    }
	});
    });
}

function checkIfUserExists(error, callback, username) {
    var sql = 'SELECT value FROM prosody WHERE "user"=\'' + username + '\'';
    pg.connect(connection_string, function(err, client, done) {
	if (err) return error(err);
	
	client.query(sql, function(err, result) {
	    done();
	    if (err) return error(err);

	    if (typeof result.rows[0] === 'undefined') {
		callback(false);
	    } else {
		callback(true);
	    }
	});
    });
}

function authorizeUser(error, callback, username, password) {
    function onError(err) {
	error(err);
    }

    function onSuccess(db_password) {
	if (password === db_password) {
	    callback();
	} else {
	    error(new utils.PasswordError());
	}
    }
    
    getPassword(onError, onSuccess, username);
}

exports.authorizeUser = authorizeUser;
exports.checkIfUserExists = checkIfUserExists;
